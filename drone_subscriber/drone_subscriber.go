package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
)

var (
	upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	redisServerAddr      = "redis:6379"
	flightPatternChannel = "flight_pattern"
	coordinatesChannel   = "drone_coordinates"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		handleWebSocket(ctx, w, r)
	})

	server := &http.Server{Addr: ":8080"}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigChan
		log.Println("Shutdown signal received... shutting down.")
		if err := server.Shutdown(ctx); err != nil {
			log.Println("Error occurred while shutting down the server: ", err)
		}
		cancel()
	}()

	log.Println("Starting server at port 8080...")
	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatalln("An error occurred while starting the server: ", err)
	}
}

func handleWebSocket(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("WebSocket experienced an error while upgrading: ", err)
		return
	}

	defer conn.Close()

	rdb := redis.NewClient(&redis.Options{
		Addr: redisServerAddr,
	})

	rdbClient := rdb.Subscribe(ctx, coordinatesChannel)
	defer rdbClient.Close()
	ch := rdbClient.Channel()

	go func() {
		for {
			select {
			case msg, ok := <-ch:
				if !ok {
					return
				}

				// Forward drone coordinates to the frontend
				if err := conn.WriteMessage(websocket.TextMessage, []byte(msg.Payload)); err != nil {
					handleWebSocketWriteError(err)
					return
				}
			case <-ctx.Done():
				return
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		default:
			if message, err := readWebSocketMessage(conn); err == nil {
				handleFlightPatternUpdate(ctx, rdb, message)
			} else {
				handleWebSocketReadError(err)
				return
			}
		}
	}
}

func readWebSocketMessage(conn *websocket.Conn) (string, error) {
	_, message, err := conn.ReadMessage()
	if err != nil {
		return "", err
	}
	return string(message), nil
}

func handleFlightPatternUpdate(ctx context.Context, rdb *redis.Client, newPattern string) {
	switch newPattern {
	case "figure-eight", "circle", "zigzag", "stationary":
		if err := rdb.Publish(ctx, flightPatternChannel, newPattern).Err(); err != nil {
			// TODO: Retry
			log.Println("Error publishing flight pattern: ", err)
		}
	default:
		log.Println("Invalid flight pattern command, maintaining current flight path.")
	}
}

func handleWebSocketReadError(err error) {
	log.Println("WebSocket error while reading message: ", err)
}

func handleWebSocketWriteError(err error) {
	if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
		// TODO: Retry
		log.Println("WebSocket experienced an unexpected close error on write: ", err)
	} else {
		log.Println("WebSocket experienced an error on write: ", err)
	}
}
