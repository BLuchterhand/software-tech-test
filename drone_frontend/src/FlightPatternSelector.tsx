import React, { useState } from "react";

interface FlightPatternSelectorProps {
  sendPattern: (pattern: string) => void;
}

const FlightPatternSelector: React.FC<FlightPatternSelectorProps> = ({ sendPattern }) => {
  const [selectedPattern, setSelectedPattern] = useState("");

  const handlePatternChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const pattern = e.target.value;
    setSelectedPattern(pattern);
    sendPattern(pattern);
  };

  return (
    <div>
      <h2>Select Flight Pattern:</h2>
      <select value={selectedPattern} onChange={handlePatternChange}>
        <option value="">Select a pattern</option>
        <option value="figure-eight">Figure-8</option>
        <option value="circle">Circle</option>
        <option value="zigzag">Zig-Zag</option>
        <option value="stationary">Stationary</option>
      </select>
    </div>
  );
};

export default FlightPatternSelector;
