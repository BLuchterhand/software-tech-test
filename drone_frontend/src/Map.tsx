import React, { FC } from "react";
import styled from "@emotion/styled";

import "./Map.css";
import { MapContainer, TileLayer } from "react-leaflet";
import { MarkerLayer, Marker } from "react-leaflet-marker";
import DroneMarker from "./DroneMarker";
import Location from "./Location";

interface MapProps {
  websocketData: Location | null;
}

const StyledMap = styled(MapContainer)`
  height: calc(100vh);
  position: relative;
`;

const Map: FC<MapProps> = ({ websocketData }) => {
  return (
    <StyledMap 
      center={[-33.946765, 151.1796423]} 
      zoom={14} 
      scrollWheelZoom={false}>
      <TileLayer attribution="" url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      <MarkerLayer>
        {websocketData && (
          <Marker position={[websocketData.latitude, websocketData.longitude]}>
            <DroneMarker />
          </Marker>
        )}
      </MarkerLayer>
    </StyledMap>
  );
};

export default Map;
