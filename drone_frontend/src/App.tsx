import React, { useEffect, useState } from "react";
import Map from "./Map";
import Location from "./Location";
import FlightPatternSelector from "./FlightPatternSelector";

const App: React.FC = () => {
  const [websocket, setWebsocket] = useState<WebSocket | null>(null);
  const [websocketData, setWebsocketData] = useState<Location | null>(null);

  useEffect(() => {
    const ws = new WebSocket("ws://localhost:8080");
    setWebsocket(ws);

    ws.onopen = () => {
      console.log("Web Socket connected.");
    };

    ws.onmessage = (event) => {
      const data = JSON.parse(event.data);
      const location = new Location(data.latitude, data.longitude);
      console.log("Coordinates: ", location);
      setWebsocketData(location);
    };

    return () => {
      ws.close();
    };
  }, []);

  const sendPattern = (pattern: string) => {
    if (websocket?.readyState === WebSocket.OPEN) {
      websocket.send(pattern);
    }
  };

  return (
    <div>
      <FlightPatternSelector sendPattern={sendPattern} />
      <Map websocketData={websocketData} />
    </div>
  );
};

export default App;
