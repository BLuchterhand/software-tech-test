package main

import (
	"context"
	"encoding/json"
	"log"
	"math"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-redis/redis/v8"
)

// TODO: Organize out models and constants into a models file/folder
type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type FlightPattern string

const (
	UnknownFlightPattern FlightPattern = ""
	FigureEight          FlightPattern = "figure-eight"
	Circle               FlightPattern = "circle"
	ZigZag               FlightPattern = "zigzag"
	Stationary           FlightPattern = "stationary"
)

var FlightPatternMap = map[string]FlightPattern{
	"figure-eight": FigureEight,
	"circle":       Circle,
	"zigzag":       ZigZag,
	"stationary":   Stationary,
}

const (
	redisServerAddr      = "redis:6379"
	flightPatternChannel = "flight_pattern"
	coordinatesChannel   = "drone_coordinates"
	sleepDuration        = time.Second
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	rdb := redis.NewClient(&redis.Options{
		Addr: redisServerAddr,
	})

	rdbClient := rdb.Subscribe(ctx, flightPatternChannel)
	defer rdbClient.Close()

	// default to stationary flight pattern on initialization
	currentFlightPattern := Stationary

	go handleFlightPatternCommands(ctx, rdbClient.Channel(), &currentFlightPattern)
	go handleShutdownSignal(ctx, cancel, rdb)

	for {
		coordinates, _ := json.Marshal(simulateFlight(currentFlightPattern))

		err := rdb.Publish(ctx, coordinatesChannel, coordinates).Err()
		if err != nil {
			log.Println("Error publishing drone coordinates: ", err)
		}

		time.Sleep(sleepDuration)
	}
}

func handleFlightPatternCommands(ctx context.Context, ch <-chan *redis.Message, currentFlightPattern *FlightPattern) {
	for {
		select {
		case msg, ok := <-ch:
			if !ok {
				return
			}

			if flightPattern, ok := FlightPatternMap[msg.Payload]; ok {
				*currentFlightPattern = flightPattern
				continue
			}

			*currentFlightPattern = Stationary
		case <-ctx.Done():
			// ensure graceful shutdown when context cancels
			return
		}
	}
}

func handleShutdownSignal(ctx context.Context, cancelFunc context.CancelFunc, rdb *redis.Client) {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	<-sigChan
	log.Println("Shutdown signal received... shutting down.")
	if err := rdb.Shutdown(ctx); err != nil {
		log.Println("Error occurred while shutting down the server: ", err)
	}
	cancelFunc()
}

func simulateFlight(currentFlightPattern FlightPattern) Location {
	centerLatitude := -33.947346
	centerLongitude := 151.179428
	frequency := 0.05

	t := float64(time.Now().UnixNano()) / float64(time.Second)

	var x, y float64

	// TODO: Switch to some flight config map for efficiency and readability
	switch currentFlightPattern {
	case FigureEight:
		amplitudeX := 0.01
		amplitudeY := 0.005
		x = amplitudeX * math.Sin(2*math.Pi*frequency*t)
		y = amplitudeY * math.Sin(4*math.Pi*frequency*t)
	case Circle:
		radius := 0.01
		angularSpeed := 0.1
		x = radius * math.Cos(angularSpeed*t)
		y = radius * math.Sin(angularSpeed*t)
	case ZigZag:
		amplitudeX := 0.01
		amplitudeY := 0.01
		x = amplitudeX * math.Sin(2*math.Pi*frequency*t)
		y = amplitudeY * math.Sin(2*math.Pi*frequency*t)
	default:
		// Default to stationary
		x = 0.0
		y = 0.0
	}

	location := Location{
		Latitude:  centerLatitude + x,
		Longitude: centerLongitude + y,
	}

	return location
}
